myFirstApplication = angular.module('myFirstApplication',['ngRoute']);

myFirstApplication.config(function($routeProvider) {
    $routeProvider
     .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'MainController'
            }) .when('/about', {
                templateUrl : 'pages/about.html',
                controller  : 'AboutController'
            }) .when('/blog', {
                templateUrl : 'pages/blog.html',
                controller  : 'PostController'
            }) .when('/contact', {
                templateUrl : 'pages/contact.html',
                controller  : 'AboutController'
            }).otherwise({
                redirectTo:'/'
            });
    
    
});


