myFirstApplication.directive("navigationbar", function() {
    return {
    controller:function($scope, $http){
    $http.get('lib/data/pages.json').success(function(data){
	$scope.pages = data;
});	
    },
    restrict : "E",
    //template : "<h1>Made by a navigationbar!</h1>",
    templateUrl:"pages/navbar.html",
    }

});

myFirstApplication.directive("pageTitle", function() {
    return {
    restrict : "A",
    template : "My First Application",
    }

});

