myFirstApplication.controller('MainController',function($scope){
$scope.msg = 'navbar';
$scope.heading1 = 'What We Do';
$scope.ctitle = 'Hello World!';
$scope.ctext ='This is a template for a simple marketing or informational website.';
$scope.paragraph1 = 'Having and managing a correct marketing strategy is crucial in a fast moving market.';
$scope.sliders = [
{id:1, title:'Los Angeles', text:"We had such a great time in LA!",imageUrl:'https://www.w3schools.com/bootstrap4/la.jpg' },
{id:2, title:'Chicago',  text:"Thank you, Chicago!",imageUrl:'https://www.w3schools.com/bootstrap4/chicago.jpg' },
{id:3, title:'New York',  text:"We love the Big Apple!",imageUrl:'https://www.w3schools.com/bootstrap4/ny.jpg' }
];
$scope.whatwedo = [
{id:1, title:'Web Design', text:"With supporting text below as a natural lead-in to additional content.",className:'block-4' },
{id:2, title:'Web Development',  text:"With supporting text below as a natural lead-in to additional content.",className:'block-5' },
{id:3, title:'Web Development',  text:"With supporting text below as a natural lead-in to additional content.",className:'block-6' },
{id:4, title:'Mobile & IOS Apps', text:"With supporting text below as a natural lead-in to additional content.",className:'block-1' },
{id:5, title:'API Integeration', text:"With supporting text below as a natural lead-in to additional content." ,className:'block-2'},
{id:6, title:'Support',text:"With supporting text below as a natural lead-in to additional content.",className:'block-3' }
];



$scope.blog = [
{id:1, title:'Card title 1', text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:2, title:'Card title 2',  text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:3, title:'Card title 3',  text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:4, title:'Card title 4', text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/paris.jpg' },
{id:5, title:'Card title 5', text:"Some quick example text to build on the card title and make up the bulk of the card's content.." ,imageUrl:'images/paris.jpg'},
{id:6, title:'Card title 6', text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:7, title:'Card title 7',  text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:8, title:'Card title 8',  text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/sanfran.jpg' },
{id:9, title:'Card title 9', text:"Some quick example text to build on the card title and make up the bulk of the card's content..",imageUrl:'images/paris.jpg' },
{id:10, title:'Card title 10', text:"Some quick example text to build on the card title and make up the bulk of the card's content.." ,imageUrl:'images/paris.jpg'},
];

$scope.randomize = function () { return 0.5 - Math.random(0,10);};


});

myFirstApplication.controller('AboutController',function($scope){
$scope.msg = 'navbar';
$scope.message = 'Everyone come and see how good I look!';
});

myFirstApplication.controller('PostController',function($scope, $http){
$scope.msg = 'navbar';
$scope.message = ' Blog!';
$scope.readmore = "sss";

$http.get('lib/data/posts.json').success(function(data){
	$scope.posts = data;
});

});